import socket
import time
import threading
import cv2
from threading import Thread

class Tello:
    # Send and receive commands, client socket
    UDP_IP = '192.168.10.1'
    UDP_PORT = 8889
    RESPONSE_TIMEOUT = 0.5  # in seconds
    TIME_BTW_COMMANDS = 0.5  # in seconds
    TIME_BTW_RC_CONTROL_COMMANDS = 0.5  # in seconds
    last_received_command = time.time()

    # Video stream, server socket
    VS_UDP_IP = '0.0.0.0'
    VS_UDP_PORT = 11111

    # VideoCapture object
    cap = None
    background_frame_read = None

    stream_on = False

    def __init__(self):
        self.UDP_PORT = 8889
        # To send comments
        self.address = (self.UDP_IP, self.UDP_PORT)
        self.clientSocket = socket.socket(socket.AF_INET,  # Internet
                                          socket.SOCK_DGRAM)  # UDP
        self.clientSocket.bind(('', self.UDP_PORT))  # For UDP response (receiving data)
        self.response = None
        self.stream_on = False

        self.clientSocket.sendto(b'command', self.address)
        self.clientSocket.sendto(b'streamon', self.address)

        # Run tello udp receiver on background
        thread = threading.Thread(target=self._run_udp_receiver, args=())
        thread.daemon = True
        thread.start()

    def _run_udp_receiver(self):
        """Setup drone UDP receiver. This method listens for responses of Tello. Must be run from a background thread
        in order to not block the main thread."""
        while True:
            try:
                self.response, _ = self.clientSocket.recvfrom(1024)  # buffer size is 1024 bytes
            except Exception as e:
                print(e)
                break

    def get_udp_video_address(self):
        return 'udp://@' + self.VS_UDP_IP + ':' + str(self.VS_UDP_PORT)  # + '?overrun_nonfatal=1&fifo_size=5000'

    def get_video_capture(self):
        """Get the VideoCapture object from the camera drone
        Returns:
            VideoCapture
        """

        if self.cap is None:
            self.cap = cv2.VideoCapture(self.get_udp_video_address())

        if not self.cap.isOpened():
            self.cap.open(self.get_udp_video_address())

        return self.cap

    def get_frame_read(self):
        """Get the BackgroundFrameRead object from the camera drone. Then, you just need to call
        backgroundFrameRead.frame to get the actual frame received by the drone.
        Returns:
            BackgroundFrameRead
        """
        if self.background_frame_read is None:
            self.background_frame_read = BackgroundFrameRead(self, self.get_udp_video_address()).start()
        return self.background_frame_read

class BackgroundFrameRead:
    """
    This class read frames from a VideoCapture in background. Then, just call backgroundFrameRead.frame to get the
    actual one.
    """

    def __init__(self, tello, address):
        tello.cap = cv2.VideoCapture(address)
        self.cap = tello.cap

        if not self.cap.isOpened():
            self.cap.open(address)

        self.grabbed, self.frame = self.cap.read()
        self.stopped = False

    def start(self):
        Thread(target=self.update_frame, args=()).start()
        return self

    def update_frame(self):
        while not self.stopped:
            if not self.grabbed or not self.cap.isOpened():
                self.stop()
            else:
                (self.grabbed, self.frame) = self.cap.read()

    def stop(self):
        self.stopped = True

def test_with_streaming_laptop_cam():
    addr = 'udp://192.168.1.7:5000'

    cap = cv2.VideoCapture(addr)

    if not cap.isOpened():
        cap.open(addr)

    while True:
        _, frame = cap.read()

        cv2.imshow('test', frame)

        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    client_ip = ''
    server_ip = '192.168.10.1'
    port = 8889

    vport = 11111

    addr = (server_ip, port)

    #(1) Open client socket to listen to 8889
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #(2) Bind to listen to 8889
    client.bind((client_ip, port))
    #(3) Send commands to turn on drone
    client.sendto(b'command', addr)
    client.sendto(b'streamon', addr)

    video_addr = 'udp://0.0.0.0:' + str(vport)

    cap = cv2.VideoCapture(video_addr)
    if not cap.isOpened():
        cap.open(video_addr)

    while True:
        _, frame = cap.read()

        if frame is not None:
            cv2.imshow('', frame)

        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
