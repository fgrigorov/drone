import cv2
import numpy as np
import socket
import threading
import time

#Note: Speed is within cm/s -> turn to km/h

class Drone:
    def __init__(self, my_ip, my_port, drone_ip='192.168.10.1', cmd_port=8889, video_stream_port=11111):
        self.CMD_PORT = cmd_port
        self.VIDEO_PORT = video_stream_port

        self.cmd_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.video_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.drone_addr = (drone_ip, self.CMD_PORT)
        self.video_addr = (drone_ip, self.VIDEO_PORT)

        self.frame = None
        self.response = None

        print('\nOpen sockets to my ip in order to listen!\n')
        self.cmd_socket.bind((my_ip, self.CMD_PORT))
        self.video_socket.bind((my_ip, self.VIDEO_PORT))

        self._init_communication()

        print('\nStart listening to responses from drone!\n')
        self.cmd_rx_thread = threading.Thread(target=self._rx_cmd)
        self.cmd_rx_thread.daemon = True

        ip_addr = 'udp://@0.0.0.0:11111'
        print('\nHooking to {}\n'.format(ip_addr))
        self.capture_device = cv2.VideoCapture(ip_addr)

        if not self.capture_device.isOpened():
            self.capture_device.open()

        print('\nStart listening to vdeio streaming from drone!\n')
        self.video_rx_thread = threading.Thread(target=self._rx_video)
        self.video_rx_thread.deamon = True

        self.cmd_rx_thread.start()
        self.video_rx_thread.start()

    def __del__(self):
        self.cmd_socket.close()
        self.video_socket.close()

    def send_command(self, cmd):
        print('Tx >> \'%s\'!\n' % cmd)

        cmd = cmd.encode('utf-8')

        timeout = 3.0  #seconds
        is_stopped = False
        timer = threading.Timer(timeout, is_stopped)

        #Note: Try sending for a certain timeout
        self.cmd_socket.sendto(cmd, self.drone_addr)
        
        timer.start()
        while self.response is None:
            if is_stopped == True:
                break    
        timer.cancel()

        if self.response is None:
            return 'No data'
        response = self.response.decode('utf-8')
        self.response = None

        return response

    def grab_frame(self):
        return self.frame if self.frame is not None else None

    def _init_communication(self):
        self.cmd_socket.sendto(b'command', self.drone_addr)
        self.video_socket.sendto(b'streamon', self.drone_addr)

    def _rx_cmd(self):
        while True:
            try:
                len_data = 3000
                self.response, ip = self.cmd_socket.recvfrom(len_data)
            except socket.error as err:
                print('Error (command): {}'.format(err))

    def _rx_video(self):
        while True:
            try:
                _, self.frame = self.capture_device.read()
                print(self.frame)
            except socket.error as err:
                print('Error (video): {}'.format(err))

if __name__ == '__main__':
    drone = Drone(my_ip='', my_port=8889)
    response = drone.send_command('battery?')
    print('Rx: ', response)

    cv2.namedWindow('Stream')

    while True:
        frame = drone.grab_frame()

        if frame is not None:
            cv2.imshow('Stream', frame)

        #'q' -> 01110001 (& 0xFF masks everything but the last 8 bits which are used in this case)
        if cv2.waitKey(1) & 0xff == ord('q'):
            print('\nQuitting!\n')
            break

    cv2.destroyAllWindows()
